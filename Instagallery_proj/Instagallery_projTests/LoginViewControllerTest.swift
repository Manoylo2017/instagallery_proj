//
//  LoginViewControllerTest.swift
//  Instagallery_projTests
//
//  Created by Developer on 7/16/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import Foundation
import XCTest
@testable import Instagallery_proj

class LoginViewControllerTest: XCTestCase {
    
    func test_viewDidLoad_webViewLoadRequest() {
        let loginVC = LoginViewController()
        let _ = loginVC.view       
        XCTAssertNotNil(loginVC.loginWebView?.url)
    }

    
}
