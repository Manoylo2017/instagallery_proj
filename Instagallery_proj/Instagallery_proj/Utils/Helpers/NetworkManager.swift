//
//  NetworkManager.swift
//  Instagallery_proj
//
//  Created by Developer on 7/18/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {

    // Checks networo reachability state
    static func isReachable() -> Bool {
        var isReachable = false
        if let networkManager = NetworkReachabilityManager() {
           isReachable = networkManager.isReachable
        }
        return isReachable
    }
}
